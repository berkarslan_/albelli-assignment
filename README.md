# README #

## Albelli - Senior Software Engineer Technical Assignment ##

### How to set up and test ###

* Get latest version
* Build the project
* In "Package Manager Console", type the following to create and seed the database: update-database -ProjectName:"AlbelliAssignment.WebAPI.Data" -StartUpProjectName:"AlbelliAssignment.WebAPI"
* Run AlbelliAssignment.DummyAuthorizationServer and AlbelliAssignment.WebAPI projects
* Open the following url to open Swagger UI interface: http://localhost:27675/swagger/ui/index
* Click on an action to test. 
* On the right side of the action, there should be a red icon, click there to authenticate through authorization server and get access_token to access Web API resources. 
* When the popup for authorization server gets opened
	* Type **any user name** (there is no username check, the authorization server is a dummy one)
	* As for password, you should type: "**123456**"
	* Select "Request Body" value from the popup to be able to enter client id and secret
	* Type "**Albelli**" for client id.
	* Type "**Secret**" for client secret.
	* Click Authorize.
* You should have got the access token now. 
* Fill the required parameters for the action you selected (if any) and send the request.

### Summary ###

* Web API side of the project is designed in a way that it is decoupled and maintainable by using SOLID principles, appropriate design patterns and best practices.
	* AlbelliAssignment.WebAPI.Business: Concrete business classes that implement the Contract Interfaces.
	* AlbelliAssignment.WebAPI.Business.Contracts: Clients reference this project to be able to access business side by IoC (They don't reference AlbelliAssignment.WebAPI.Business).
	* AlbelliAssignment.WebAPI.Common: Contains common classes to be used across all of the projects.
	* AlbelliAssignment.WebAPI.Common.Contracts: Contains common interfaces and constants
	* AlbelliAssignment.WebAPI.Data: Contains concrete data classes.
	* AlbelliAssignment.WebAPI.Data.Contracts: Contains data class interfaces which are used to access database side.
	* AlbelliAssignment.WebAPI.Data.Entities: Contains database entities.
	* AlbelliAssignment.WebAPI.IoC: Contains IoC specific classes.
	* AlbelliAssignment.WebAPI.Tests: Tests for WebAPI project and it's dependencies.
	* AlbelliAssignment.WebAPI.Utilities: This project references all of the projects so that it can register interfaces with their concrete implementations. 
	* AlbelliAssignment.WebAPI.Utilities.Contracts: Contains some interfaces to be used.  
* Authorization Server is a dummy one. It doesn't access database but only validates the input by some constants and gives the token if they are valid. It only supports password grant of OAuth2.
* Unit tests doesn't cover Data project because of time limitations. Because of that reason, a few unit test classes work in a way like integration tests. However, after mocking of the Data side's classes are done, they will work as intended after registering their mocked implementations through DI. (I will mock the data side if I find some free time too.)