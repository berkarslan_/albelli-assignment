﻿using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Contracts.Context
{
    /// <summary>
    /// Data Context for ORM
    /// </summary>
    public interface IDataContext : IDisposable
    {
        /// <summary>
        /// This is used to insert/update/delete an entity
        /// </summary>
        /// <typeparam name="TEntity">Entity type to manage</typeparam>
        /// <param name="entity">Entity to manage</param>
        /// <returns>Database entry to save</returns>
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : ModelBase;

        /// <summary>
        /// Saves pending changes
        /// </summary>
        /// <returns>The number of objects written to the underlying database</returns>
        int SaveChanges();

        /// <summary>
        /// Saves pending changes asynchronously
        /// </summary>
        /// <returns>The number of objects written to the underlying database.</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Raw set for an entity to manage
        /// </summary>
        /// <typeparam name="TEntity">Entity type to get the set for</typeparam>
        /// <returns>Set for the entity</returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : ModelBase;
    }
}
