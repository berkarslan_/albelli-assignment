﻿using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Contracts.Repository
{
    /// <summary>
    /// This generic repository is used to access the database. 
    /// It also implements <see cref="IQueryable{TEntity}"/> so that querying can also be done directly without using any of the methods.
    /// </summary>
    /// <typeparam name="TEntity">Entity type to create the repository for</typeparam>
    public interface IGenericRepository<TEntity> : IQueryable<TEntity> where TEntity : ModelBase
    {
        /// <summary>
        /// Checks whether any records exists for the given filter asynchronously
        /// </summary>
        /// <param name="predicate">Search condition</param>
        /// <returns>True if exists; false if not</returns>
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Finds the entity with the given primary key
        /// </summary>
        /// <param name="pk">Primary key for the entity to search for</param>
        /// <returns>Entity</returns>
        TEntity Find(object pk);

        /// <summary>
        /// Finds the entity asynchronously with the given primary key
        /// </summary>
        /// <param name="pk">Primary key for the entity to search for</param>
        /// <returns>Entity</returns>
        Task<TEntity> FindAsync(object pk);

        /// <summary>
        /// Asynchronously returns the only element of a sequence, or a default value if the sequence is empty
        /// </summary>
        /// <returns>Entity</returns>
        Task<TEntity> SingleOrDefaultAsync();

        /// <summary>
        /// Asynchronously returns the list for <see cref="IQueryable"/> object
        /// </summary>
        /// <returns>List of entity</returns>
        Task<List<TEntity>> ToListAsync();

        /// <summary>
        /// Inserts the given entity
        /// </summary>
        /// <param name="entity">Entity to insert</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Inserts the given entities
        /// </summary>
        /// <param name="entities">Entities to insert</param>
        void InsertRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Updates the entity
        /// </summary>
        /// <param name="entity">Entity to update</param>
        void Update(TEntity entity);

        /// <summary>
        /// Updates the entities
        /// </summary>
        /// <param name="entity">Entities to update</param>
        void UpdateRange(IEnumerable<TEntity> entity);

        /// <summary>
        /// Deletes the entity by it's id
        /// </summary>
        /// <param name="id">Entity id to delete</param>
        void Delete(long id);

        /// <summary>
        /// Deletes the entity by it's instance
        /// </summary>
        /// <param name="entity">Entity to delete</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Deletes the entities by their instance
        /// </summary>
        /// <param name="entities">Entities to delete</param>
        void DeleteRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// Facade for applying filter, order by, include and paging with a single method
        /// </summary>
        /// <param name="filter">Filter to apply</param>
        /// <param name="orderBy">Order by clause</param>
        /// <param name="includeProperties">Properties to include when returning</param>
        /// <param name="page">Page number to skip (not the number of elements to skip)</param>
        /// <param name="pageSize">Page size to get</param>
        /// <returns><see cref="IQueryable{TEntity}"/> object which is filtered, ordered by, additional properties included and paging applied in single method. 
        /// Additional filters or other requirements can also be applied since <see cref="IQueryable{TEntity}"/> is returned</returns>
        IGenericRepository<TEntity> Get(
          Expression<Func<TEntity, bool>> filter = null,
          Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
          List<Expression<Func<TEntity, object>>> includeProperties = null,
          int? page = null,
          int? pageSize = null);
    }
}
