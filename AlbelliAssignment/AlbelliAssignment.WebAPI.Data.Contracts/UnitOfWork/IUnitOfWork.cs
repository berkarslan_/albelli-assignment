﻿using AlbelliAssignment.WebAPI.Data.Contracts.Repository;
using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork
{
    /// <summary>
    /// Unit of work which can be used to get repositories and to save changes to the database
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Saves pending changes
        /// </summary>
        /// <returns>The number of objects written to the underlying database</returns>
        int SaveChanges();

        /// <summary>
        /// Save pending changes asynchronously
        /// </summary>
        /// <returns>The number of objects written to the underlying database.</returns>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Gets repository instance for an entity type
        /// </summary>
        /// <typeparam name="TEntity">Entity type to get the repository for</typeparam>
        /// <returns>Repository for the given entity type</returns>
        IGenericRepository<TEntity> Repository<TEntity>() where TEntity : ModelBase;
    }
}
