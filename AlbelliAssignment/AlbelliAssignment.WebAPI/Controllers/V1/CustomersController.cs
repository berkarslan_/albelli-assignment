﻿using AlbelliAssignment.WebAPI.Business.Contracts.BusinessContracts;
using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using AlbelliAssignment.WebAPI.Common.Contracts.Constants;
using AlbelliAssignment.WebAPI.Utilities.Controller;
using Microsoft.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace AlbelliAssignment.WebAPI.Controllers.V1
{
    [ApiVersion("1")]
    [RoutePrefix(RouteUrlTable.Customer.RoutePrefix)]
    public class CustomersController : ApiControllerBase
    {
        [ResponseType(typeof(CustomerDTO))]
        [Route(RouteUrlTable.Customer.GetCustomerById, Name = RouteNameTable.Customer.GetCustomerById)]
        [HttpGet]
        public virtual async Task<IHttpActionResult> GetCustomerById(int id)
        {
            var result = await Proxy.ExecuteAsync<ICustomerBusiness, CustomerDTO>(a => a.GetCustomer(id));
            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [ResponseType(typeof(List<CustomerInfoDTO>))]
        [Route(RouteUrlTable.Customer.GetCustomers, Name = RouteNameTable.Customer.GetCustomers)]
        [HttpGet]
        public virtual async Task<IHttpActionResult> GetCustomers()
        {
            var result = await Proxy.ExecuteAsync<ICustomerBusiness, List<CustomerInfoDTO>>(a => a.GetCustomers());
            return Ok(result);
        }

        [ResponseType(typeof(CustomerDTO))]
        [Route(RouteUrlTable.Customer.SaveCustomer, Name = RouteNameTable.Customer.SaveCustomer)]
        [HttpPost]
        public virtual async Task<IHttpActionResult> Post(CustomerInfoDTO dto)
        {
            //Check if customer with the same email address already exists
            var exists = await Proxy.ExecuteAsync<ICustomerBusiness, bool>(a => a.CustomerExists(dto.Email));
            if (exists)
                return BadRequest("A customer with the same email address already exists");

            //Save and return
            var customerDto = Mapper.Map<CustomerInfoDTO, CustomerDTO>(dto);
            await Proxy.ExecuteAsync<ICustomerBusiness>(a => a.AddCustomer(customerDto));
            return CreatedAtRoute(RouteNameTable.Customer.GetCustomerById, new { id = customerDto.Id }, customerDto);
        }

        [ResponseType(typeof(OrderDTO))]
        [Route(RouteUrlTable.Customer.SaveOrderForCustomer, Name = RouteNameTable.Customer.SaveOrderForCustomer)]
        [HttpPost]
        public virtual async Task<IHttpActionResult> Orders(int id, OrderInfoDTO dto)
        {
            //Normalize DTO
            var orderDto = Mapper.Map<OrderInfoDTO, OrderDTO>(dto);
            orderDto.CustomerId = id;
            orderDto.CreatedDate = DateTime.Now;

            //Save and return
            await Proxy.ExecuteAsync<ICustomerBusiness>(a => a.AddOrder(orderDto));
            return CreatedAtRoute(RouteNameTable.Customer.GetCustomerById, new { id = id }, orderDto);
        }
    }
}
