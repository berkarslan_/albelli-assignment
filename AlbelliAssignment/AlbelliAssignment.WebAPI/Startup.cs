﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using AlbelliAssignment.WebAPI.Utilities.Registrations;

[assembly: OwinStartup(typeof(AlbelliAssignment.WebAPI.Startup))]

namespace AlbelliAssignment.WebAPI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Registrar.RegisterOwin(app);
        }
    }
}
