﻿using AlbelliAssignment.WebAPI.Utilities.Registrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace AlbelliAssignment.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Registrar.RegisterGlobal();
        }
    }
}
