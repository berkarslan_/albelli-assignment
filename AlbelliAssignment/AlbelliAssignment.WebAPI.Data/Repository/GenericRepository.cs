﻿using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.Repository;
using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Repository
{
    public class GenericRepository<TEntity> : 
        IDbAsyncEnumerable<TEntity>,
        IGenericRepository<TEntity> where TEntity : ModelBase
    {
        private readonly IDataContext context;
        private readonly DbSet<TEntity> dbSet;
        private IQueryable<TEntity> iQueryableEntity;

        public GenericRepository(IDataContext context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
            iQueryableEntity = this;
        }

        public Type ElementType
        {
            get
            {
                return typeof(TEntity);
            }
        }

        public Expression Expression
        {
            get
            {
                return dbSet.AsNoTracking().AsQueryable().Expression;
            }
        }

        public IQueryProvider Provider
        {
            get
            {
                return dbSet.AsNoTracking().AsQueryable().Provider;
            }
        }

        public void Delete(long id)
        {
            var entity = dbSet.Find(id);
            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            var entry = context.Entry(entity);
            entry.State = EntityState.Deleted;

            if (entry.State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public TEntity Find(object pk)
        {
            return dbSet.Find(pk);
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await dbSet.AnyAsync(predicate);
        }

        public async Task<TEntity> FindAsync(object pk)
        {
            return await dbSet.FindAsync(pk);
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return dbSet.AsNoTracking().AsEnumerable().GetEnumerator();
        }

        public void Insert(TEntity entity)
        {
            var entry = context.Entry(entity);
            entry.State = EntityState.Added;

            if (entry.State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
        }

        public void InsertRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Insert(entity);
            }
        }

        public IGenericRepository<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includeProperties = null,
            int? page = default(int?),
            int? pageSize = default(int?))
        {
            if (filter != null)
                iQueryableEntity = iQueryableEntity.Where(filter);
            if (includeProperties != null)
                iQueryableEntity = includeProperties.Aggregate(iQueryableEntity, (current, include) => current.Include(include));
            if (orderBy != null)
                iQueryableEntity = orderBy(iQueryableEntity);
            if (page != null && pageSize != null)
                iQueryableEntity = iQueryableEntity.Skip((page.Value - 1) * pageSize.Value).Take(pageSize.Value);

            return this;
        }

        public async Task<TEntity> SingleOrDefaultAsync()
        {
            return await iQueryableEntity.SingleOrDefaultAsync();
        }

        public async Task<List<TEntity>> ToListAsync()
        {
            return await iQueryableEntity.ToListAsync();
        }

        public void Update(TEntity entity)
        {
            var entry = context.Entry(entity);
            entry.State = EntityState.Modified;

            if (entry.State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return dbSet.AsNoTracking().AsEnumerable().GetEnumerator();
        }

        public IDbAsyncEnumerator<TEntity> GetAsyncEnumerator()
        {
            return ((IDbAsyncEnumerable<TEntity>)dbSet).GetAsyncEnumerator();
        }

        IDbAsyncEnumerator IDbAsyncEnumerable.GetAsyncEnumerator()
        {
            return this.GetAsyncEnumerator();
        }
    }
}
