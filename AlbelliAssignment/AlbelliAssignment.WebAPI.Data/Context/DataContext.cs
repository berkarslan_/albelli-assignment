﻿using AlbelliAssignment.WebAPI.Common.Contracts.Constants;
using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Entities.Base;
using AlbelliAssignment.WebAPI.Data.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Context
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext()
            : base(SystemConstants.AppSettings.DefaultConnectionName)
        {
            //Disable Lazy Loading (personal preference)
            Configuration.LazyLoadingEnabled = false;

            //Disable Proxy Creation to prevent serialization issues
            Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }

        public new DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : ModelBase
        {
            return base.Entry(entity);
        }

        public new DbSet<TEntity> Set<TEntity>() where TEntity : ModelBase
        {
            return base.Set<TEntity>();
        }
    }
}
