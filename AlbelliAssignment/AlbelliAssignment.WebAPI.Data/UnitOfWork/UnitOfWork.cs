﻿using AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlbelliAssignment.WebAPI.Data.Contracts.Repository;
using AlbelliAssignment.WebAPI.Data.Entities.Base;
using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Repository;

namespace AlbelliAssignment.WebAPI.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDataContext context;

        public UnitOfWork(IDataContext context)
        {
            this.context = context;
        }

        public IGenericRepository<TEntity> Repository<TEntity>() where TEntity : ModelBase
        {
            return new GenericRepository<TEntity>(context);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
