namespace AlbelliAssignment.WebAPI.Data.Migrations
{
    using Common.Helpers;
    using Entities.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<AlbelliAssignment.WebAPI.Data.Context.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations";
        }

        protected override void Seed(AlbelliAssignment.WebAPI.Data.Context.DataContext context)
        {
            context.Customer.AddOrUpdate(a => a.Email,
                new Customer()
                {
                    Email = "berkarslan.cs@gmail.com",
                    Name = "Berk Arslan",
                    Orders = new List<Order>()
                    {
                        new Order()
                        {
                            CreatedDate = DateTime.Now,
                            Price = RandomGenerator.GetRandomNumber(0, 10000)
                        },
                        new Order()
                        {
                            CreatedDate = DateTime.Now,
                            Price = RandomGenerator.GetRandomNumber(0, 10000)
                        }
                    }
                },
                new Customer()
                {
                    Email = "asena@celtikcioglu.com",
                    Name = "Asena Celtikcioglu",
                    Orders = new List<Order>()
                    {
                        new Order()
                        {
                            CreatedDate = DateTime.Now,
                            Price = RandomGenerator.GetRandomNumber(0, 10000)
                        },
                        new Order()
                        {
                            CreatedDate = DateTime.Now,
                            Price = RandomGenerator.GetRandomNumber(0, 10000)
                        },
                        new Order()
                        {
                            CreatedDate = DateTime.Now,
                            Price = RandomGenerator.GetRandomNumber(0, 10000)
                        }
                    }
                }
            );
        }
    }
}
