﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using AlbelliAssignment.WebAPI.Common.Contracts.Mapper;
using AlbelliAssignment.WebAPI.IoC.Factory;

namespace AlbelliAssignment.WebAPI.Utilities.Mapper
{
    internal class AutoMapperWrapped : Common.Contracts.Mapper.IMapper
    {
        private static readonly AutoMapper.IMapper mapper;

        static AutoMapperWrapped()
        {
            var container = ContainerFactory.GetContainer();
            var config = container.Resolve<MapperConfiguration>();
            mapper = config.CreateMapper();
        }

        public TTo Map<TFrom, TTo>(TFrom value)
        {
            return mapper.Map<TFrom, TTo>(value);
        }
    }
}
