﻿using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using AlbelliAssignment.WebAPI.Data.Entities.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Utilities.Mapper
{
    public class DefaultMappingProfile : Profile
    {
        public DefaultMappingProfile()
        {
            //DTO to DTO
            CreateMap<CustomerInfoDTO, CustomerDTO>().ReverseMap();
            CreateMap<OrderInfoDTO, OrderDTO>().ReverseMap();

            //Entity to DTO and vice versa
            CreateMap<Customer, CustomerDTO>().ReverseMap();
            CreateMap<Customer, CustomerInfoDTO>().ReverseMap();
            CreateMap<Order, OrderDTO>().ReverseMap();
            CreateMap<Order, OrderInfoDTO>().ReverseMap();
        }
    }
}
