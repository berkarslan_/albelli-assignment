﻿using AlbelliAssignment.WebAPI.Business.Base;
using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using AlbelliAssignment.WebAPI.Common.Contracts.Constants;
using AlbelliAssignment.WebAPI.Data.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork;
using AlbelliAssignment.WebAPI.Data.UnitOfWork;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Utilities.Attributes.FilterAttributes;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using AlbelliAssignment.WebAPI.Utilities.IoC;
using AlbelliAssignment.WebAPI.Utilities.Mapper;
using AlbelliAssignment.WebAPI.Utilities.Proxy;
using AutoMapper;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Web.Http.Routing;
using Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Routing;
using Swashbuckle.Application;
using AlbelliAssignment.WebAPI.Utilities.Swagger;
using System.Web.Http.Description;

namespace AlbelliAssignment.WebAPI.Utilities.Registrations
{
    public static class Registrar
    {
        /// <summary>
        /// Main entry point for Global.asax
        /// </summary>
        public static void RegisterGlobal()
        {
            LoadNotUsedDirectlyAssemblies();
            RegisterWebAPIComponents();
            ConfigureDIRegistrations();
        }

        /// <summary>
        /// Main entry point for OWIN
        /// </summary>
        /// <param name="app"></param>
        public static void RegisterOwin(IAppBuilder app)
        {
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        internal static void LoadNotUsedDirectlyAssemblies()
        {
            //Type hacks - The following lines ensure that the DLLs belonging to the types get copied under bin directory
            var instance = SqlProviderServices.Instance;
            var typeHack = typeof(BusinessBase);
        }

        internal static void RegisterWebAPIComponents()
        {
            //Web API Specific Registrations
            GlobalConfiguration.Configure((config) =>
            {
                //Web API Configuration and Services
                var container = ContainerFactory.GetContainer();
                config.DependencyResolver = new WebAPIResolver(container);
                config.AddRoutingWithVersioning()
                    .AddFilters()
                    .EnableSwagger();
            });
        }

        internal static void ConfigureDIRegistrations()
        {
            //Get AutoMapper configuration instance
            MapperConfiguration autoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DefaultMappingProfile>();
            });

            //DI Registrations
            var container = ContainerFactory.GetContainer();
            container.Register<IDataContext, DataContext>();
            container.Register<IUnitOfWork, UnitOfWork>();
            container.Register<IProxy, BusinessProxy>();
            container.RegisterInstance(autoMapperConfig);
            container.Register<Common.Contracts.Mapper.IMapper, AutoMapperWrapped>(Lifetime.Singleton);
            container.RegisterBusinessContracts();
        }

        internal static HttpConfiguration AddRoutingWithVersioning(this HttpConfiguration config)
        {
            //Web API Routes and Versioning
            var constraintResolver = new DefaultInlineConstraintResolver()
            {
                ConstraintMap =
                    {
                        ["apiVersion"] = typeof(ApiVersionRouteConstraint)
                    }
            };
            config.MapHttpAttributeRoutes(constraintResolver);
            config.AddApiVersioning(o => o.ReportApiVersions = true);

            return config;
        }

        internal static HttpConfiguration AddFilters(this HttpConfiguration config)
        {
            //Web API Filters
            config.Filters.Add(new ModelStateValidateAttribute());
            return config;
        }

        internal static HttpConfiguration EnableSwagger(this HttpConfiguration config)
        {
            //Enable API Explorer, Swagger and Swagger UI
            var apiExplorer = config.AddVersionedApiExplorer(o => o.GroupNameFormat = "'v'V");
            config.EnableSwagger("{apiVersion}/swagger", swagger =>
            {
                //Enable multiple api versions
                swagger.MultipleApiVersions((apiDescription, version) => apiDescription.GetGroupName() == version,
                    info =>
                    {
                        foreach (var group in apiExplorer.ApiDescriptions)
                        {
                            var description = "Albelli Assignment";

                            if (group.IsDeprecated)
                            {
                                description += " (This API version has been deprecated)";
                            }

                            info.Version(group.Name, $"Albelli API {group.Name}")
                                .Description(description);
                        }
                    });

                //Enable OAuth2 authentication of swagger
                swagger.OAuth2("oauth2")
                    .Description("OAuth2 Resource Owner Password Credential Grant")
                    .Flow("password")
                    .AuthorizationUrl(new Uri(new Uri(RouteUrlTable.OAuth.AuthorizationServerBaseAddress), RouteUrlTable.OAuth.AuthorizePath).ToString())
                    .TokenUrl(new Uri(new Uri(RouteUrlTable.OAuth.AuthorizationServerBaseAddress), RouteUrlTable.OAuth.TokenPath).ToString());

                //Group actions by their name and http method
                swagger.GroupActionsBy(desc => $"{desc.ActionDescriptor.ControllerDescriptor.ControllerName} - {desc.HttpMethod.Method}");

                //Add custom operation filters
                swagger.OperationFilter<SwaggerDefaultValuesFilter>();
                swagger.OperationFilter<OAuth2SecurityRequirementsFilter>();
            })
            .EnableSwaggerUi(c =>
            {
                //Enable OAuth2 authentication for Swagger UI
                c.EnableOAuth2Support(SystemConstants.AppSettings.ClientId, SystemConstants.AppSettings.ClientSecret, "swagger-realm", "Swagger UI");

                //Enable multiple version url selector for Swagger UI
                c.EnableDiscoveryUrlSelector();
            });

            return config;
        }

        internal static void RegisterBusinessContracts(this IContainer container)
        {
            //Business Contract Registrations
            //Get all business classes
            var businesses = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(w => typeof(IBusiness).IsAssignableFrom(w) && w.IsClass && !w.IsAbstract)
                .ToList();

            //Inject business contracts with their implementations
            businesses.ForEach(business =>
            {
                container.Register(business.GetInterfaces().First(f => typeof(IBusiness).IsAssignableFrom(f) && f != typeof(IBusiness)), business);
            });
        }
    }
}
