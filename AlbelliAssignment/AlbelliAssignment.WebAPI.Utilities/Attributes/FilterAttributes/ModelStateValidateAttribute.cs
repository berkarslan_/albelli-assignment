﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;

namespace AlbelliAssignment.WebAPI.Utilities.Attributes.FilterAttributes
{
    /// <summary>
    /// Automatically validates model state when applied to an action/controller
    /// </summary>
    public class ModelStateValidateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if ((actionContext.Request.Method == HttpMethod.Post || actionContext.Request.Method == HttpMethod.Put) &&
                actionContext.ModelState.IsValid == false)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            }
        }
    }
}
