﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Utilities.Proxy
{
    internal class BusinessProxy : IProxy
    {
        private readonly IContainer container = ContainerFactory.GetContainer();

        public void Execute<TBusiness>(Action<TBusiness> call) where TBusiness : IBusiness
        {
            var business = container.Resolve<TBusiness>();
            call(business);
        }

        public TResult Execute<TBusiness, TResult>(Func<TBusiness, TResult> call) where TBusiness : IBusiness
        {
            var business = container.Resolve<TBusiness>();
            return call(business);
        }

        public async Task ExecuteAsync<TBusiness>(Func<TBusiness, Task> call) where TBusiness : IBusiness
        {
            var business = container.Resolve<TBusiness>();
            await call(business);
        }

        public async Task<TResult> ExecuteAsync<TBusiness, TResult>(Func<TBusiness, Task<TResult>> call) where TBusiness : IBusiness
        {
            var business = container.Resolve<TBusiness>();
            return await call(business);
        }
    }
}
