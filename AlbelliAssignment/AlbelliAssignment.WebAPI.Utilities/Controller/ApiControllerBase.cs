﻿using AlbelliAssignment.WebAPI.Common.Contracts.Mapper;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Utilities.Attributes.FilterAttributes;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Controller;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace AlbelliAssignment.WebAPI.Utilities.Controller
{
    /// <summary>
    /// Base class for Web API controllers
    /// </summary>
    [Authorize]
    [ModelStateValidate]
    public abstract class ApiControllerBase : ApiController, IApiController
    {
        private readonly IContainer container = ContainerFactory.GetContainer();

        private IProxy proxy;
        protected IProxy Proxy
        {
            get
            {
                if (proxy == null)
                {
                    proxy = container.Resolve<IProxy>();
                }
                return proxy;
            }
        }

        private IMapper mapper;
        protected IMapper Mapper
        {
            get
            {
                if (mapper == null)
                {
                    mapper = container.Resolve<IMapper>();
                }
                return mapper;
            }
        }
    }
}
