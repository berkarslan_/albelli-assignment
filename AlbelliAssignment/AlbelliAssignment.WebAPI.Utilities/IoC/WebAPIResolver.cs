﻿using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace AlbelliAssignment.WebAPI.Utilities.IoC
{
    internal class WebAPIResolver : IDependencyResolver
    {
        protected readonly IContainer Container;

        public WebAPIResolver(IContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException(nameof(container));
            }
            Container = container;
        }

        public object GetService(Type serviceType)
        {
            try
            {
                return Container.Resolve(serviceType);
            }
            catch (FailedResolutionException)
            {
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return Container.ResolveAll(serviceType);
            }
            catch (FailedResolutionException)
            {
                return new List<object>();
            }
        }

        public IDependencyScope BeginScope()
        {
            var child = Container.CreateChildContainer();
            return new WebAPIResolver(child);
        }

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}
