﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy
{
    /// <summary>
    /// Proxy to access business side
    /// </summary>
    public interface IProxy
    {
        /// <summary>
        /// Execute call to business side
        /// </summary>
        /// <typeparam name="TBusiness">Business interface type</typeparam>
        /// <typeparam name="TResult">Business method return type</typeparam>
        /// <param name="call">Business function</param>
        /// <returns>Business method return type</returns>
        TResult Execute<TBusiness, TResult>(Func<TBusiness, TResult> call) where TBusiness : IBusiness;

        /// <summary>
        /// Execute call to void business side
        /// </summary>
        /// <typeparam name="TBusiness">Business interface type</typeparam>
        /// <param name="call">Business function</param>
        void Execute<TBusiness>(Action<TBusiness> call) where TBusiness : IBusiness;

        /// <summary>
        /// Async execute call to business side with no return value
        /// </summary>
        /// <typeparam name="TBusiness">Business interface type</typeparam>
        /// <param name="call">Business function</param>
        /// <returns>Task</returns>
        Task ExecuteAsync<TBusiness>(Func<TBusiness, Task> call) where TBusiness : IBusiness;

        /// <summary>
        /// Async execute call to business side
        /// </summary>
        /// <typeparam name="TBusiness">Business interface type</typeparam>
        /// <typeparam name="TResult">Business method return type</typeparam>
        /// <param name="call">business function</param>
        /// <returns>Business method return type</returns>
        Task<TResult> ExecuteAsync<TBusiness, TResult>(Func<TBusiness, Task<TResult>> call) where TBusiness : IBusiness;
    }
}
