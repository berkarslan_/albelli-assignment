﻿using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Utilities.Contracts.Controller
{
    /// <summary>
    /// Web API controllers' interface
    /// </summary>
    public interface IApiController
    {
    }
}
