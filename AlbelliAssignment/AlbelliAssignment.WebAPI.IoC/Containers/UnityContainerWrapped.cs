﻿using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Exceptions;
using AlbelliAssignment.WebAPI.IoC.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Unity.Exceptions;
using Unity.Lifetime;

namespace AlbelliAssignment.WebAPI.IoC.Containers
{
    public class UnityContainerWrapped : IContainer
    {
        protected IUnityContainer Container;

        public UnityContainerWrapped()
            : this(new UnityContainer())
        {
        }

        protected UnityContainerWrapped(IUnityContainer container)
        {
            Container = container;
        }

        public IContainer CreateChildContainer()
        {
            var child = Container.CreateChildContainer();
            return new UnityContainerWrapped(child);
        }

        public void RegisterInstance<TType>(TType instance)
        {
            Container.RegisterInstance(instance); 
        }

        public void Register<TFrom, TTo>() where TTo : TFrom
        {
            Register(typeof(TFrom), typeof(TTo));
        }

        public void Register(Type from, Type to)
        {
            Register(from, to, Lifetime.PerCall);
        }

        public void Register<TFrom, TTo>(Lifetime lifetime) where TTo : TFrom
        {
            Register(typeof(TFrom), typeof(TTo), lifetime);
        }

        public void Register(Type from, Type to, Lifetime lifetime)
        {
            Container.RegisterType(from, to, LifetimeFactory.GetLifetimeManager(lifetime));
        }

        public void RegisterNamed<TFrom, TTo>(string name) where TTo : TFrom
        {
            RegisterNamed(typeof(TFrom), typeof(TTo), name);
        }

        public void RegisterNamed(Type from, Type to, string name)
        {
            Container.RegisterType(from, to, name);
        }

        public TType Resolve<TType>()
        {
            return (TType)Resolve(typeof(TType));
        }

        public object Resolve(Type type)
        {
            try
            {
                return Container.Resolve(type);
            }
            catch (ResolutionFailedException exc)
            {
                //Throw our own exception type so that the client libraries don't need to reference Unity dlls
                throw new FailedResolutionException(exc.Message, exc.InnerException);
            }
        }

        public IEnumerable<object> ResolveAll(Type type)
        {
            return Container.ResolveAll(type);
        }

        public void Dispose()
        {
            Container.Dispose();
            Container = null;
        }
    }
}
