﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.IoC.Base
{
    /// <summary>
    /// DI registration lifetimes.
    /// Our own lifetime enum is used so that the client libraries don't need to reference thirdparty container dlls
    /// </summary>
    public enum Lifetime
    {
        PerCall,
        Singleton
    }
}
