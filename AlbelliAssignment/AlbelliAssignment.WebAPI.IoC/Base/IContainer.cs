﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Lifetime;

namespace AlbelliAssignment.WebAPI.IoC.Base
{
    /// <summary>
    /// DI Container
    /// </summary>
    public interface IContainer : IDisposable
    {
        /// <summary>
        /// Register instance to the container object
        /// </summary>
        /// <typeparam name="TType">Instance type</typeparam>
        /// <param name="instance">Instance to register</param>
        void RegisterInstance<TType>(TType instance);

        /// <summary>
        /// Register type to the container object
        /// </summary>
        /// <typeparam name="TFrom">Base type to resolve from</typeparam>
        /// <typeparam name="TTo">Type to resolve to</typeparam>
        void Register<TFrom, TTo>() where TTo : TFrom;

        /// <summary>
        /// Register type to the container object with the given lifetime
        /// </summary>
        /// <typeparam name="TFrom">Base type to resolve from</typeparam>
        /// <typeparam name="TTo">Type to resolve to</typeparam>
        /// <param name="lifetime">Lifetime to register the type</param>
        void Register<TFrom, TTo>(Lifetime lifetime) where TTo : TFrom;

        /// <summary>
        /// Non genericly register type to the container object
        /// </summary>
        /// <param name="from">Base type to resolve from</param>
        /// <param name="to">Type to resolve to</param>
        void Register(Type from, Type to);

        /// <summary>
        /// Non genericly register type to the container object with the given lifetime
        /// </summary>
        /// <param name="from">Base type to resolve from</param>
        /// <param name="to">Type to resolve to</param>
        /// <param name="lifetime">Lifetime to register the type</param>
        void Register(Type from, Type to, Lifetime lifetime);

        /// <summary>
        /// Register type to the container object with giving it name
        /// </summary>
        /// <typeparam name="TFrom">Base type to resolve from</typeparam>
        /// <typeparam name="TTo">Type to resolve to</typeparam>
        /// <param name="name">Name of the registration</param>
        void RegisterNamed<TFrom, TTo>(string name) where TTo : TFrom;

        /// <summary>
        /// Register type to the container object with giving it name non genericly
        /// </summary>
        /// <param name="from">Base type to resolve from</param>
        /// <param name="to">Type to resolve to</param>
        /// <param name="name">Name of the registration</param>
        void RegisterNamed(Type from, Type to, string name);

        /// <summary>
        /// Resolves the given type to it's registered instance
        /// </summary>
        /// <typeparam name="TType">Type to resolve</typeparam>
        /// <returns>Instance for the given type</returns>
        TType Resolve<TType>();

        /// <summary>
        /// Resolves the given type non genericly to it's registered instance
        /// </summary>
        /// <param name="type">Type to resolve</param>
        /// <returns>Instance for the given type</returns>
        object Resolve(Type type);

        /// <summary>
        /// Resolves all registered types for the given type
        /// </summary>
        /// <param name="type">Type to resolve from</param>
        /// <returns>Registered instances for the given type</returns>
        IEnumerable<object> ResolveAll(Type type);

        /// <summary>
        /// Creates child container under the parent container
        /// </summary>
        /// <returns>Child container</returns>
        IContainer CreateChildContainer();
    }
}
