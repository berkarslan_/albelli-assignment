﻿using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.IoC.Factory
{
    /// <summary>
    /// Factory to get IoC container
    /// </summary>
    public static class ContainerFactory
    {
        private static readonly Lazy<IContainer> container = new Lazy<IContainer>(() =>
        {
            var container = new UnityContainerWrapped();
            return container;
        });

        /// <summary>
        /// Get IoC container
        /// </summary>
        /// <returns>IoC container</returns>
        public static IContainer GetContainer()
        {
            return container.Value;
        }
    }
}
