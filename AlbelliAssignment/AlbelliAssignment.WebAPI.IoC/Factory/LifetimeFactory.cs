﻿using AlbelliAssignment.WebAPI.IoC.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity.Lifetime;

namespace AlbelliAssignment.WebAPI.IoC.Factory
{
    /// <summary>
    /// Factory to get lifetime manager from the given lifetime
    /// </summary>
    internal static class LifetimeFactory
    {
        /// <summary>
        /// Get lifetime manager for the lifetime
        /// </summary>
        /// <param name="lifetime">Lifetime to get the manager</param>
        /// <returns>Lifetime manager for the given lifetime</returns>
        public static LifetimeManager GetLifetimeManager(Lifetime lifetime)
        {
            if (lifetime == Lifetime.PerCall)
                return new TransientLifetimeManager();
            else if (lifetime == Lifetime.Singleton)
                return new ContainerControlledLifetimeManager();
            else
                throw new NotImplementedException();
        }
    }
}
