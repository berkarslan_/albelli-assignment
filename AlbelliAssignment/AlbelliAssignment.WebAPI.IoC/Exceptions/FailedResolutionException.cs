﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.IoC.Exceptions
{
    /// <summary>
    /// This exception is thrown when the requested type couldn't be resolved from DI Container.
    /// Our own exception type is used so that the client libraries don't need to reference thirdparty container dlls
    /// </summary>
    public class FailedResolutionException : Exception
    {
        public FailedResolutionException()
        {
        }

        public FailedResolutionException(string message)
        : base(message)
        {
        }

        public FailedResolutionException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
