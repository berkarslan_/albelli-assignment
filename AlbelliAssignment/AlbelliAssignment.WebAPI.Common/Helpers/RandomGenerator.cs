﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Common.Helpers
{
    /// <summary>
    /// Random number generator
    /// </summary>
    public static class RandomGenerator
    {
        private static readonly Random getRandom = new Random();
        private static readonly object syncLock = new object();

        /// <summary>
        /// Generates random number
        /// </summary>
        /// <param name="min">Range to start</param>
        /// <param name="max">Range to end</param>
        /// <returns>Random number</returns>
        public static int GetRandomNumber(int min, int max)
        {
            lock (syncLock)
            { 
                return getRandom.Next(min, max);
            }
        }
    }
}
