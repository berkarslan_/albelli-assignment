﻿using AlbelliAssignment.WebAPI.Tests.Fakes.Entities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Tests.Fakes.Mapping
{
    public class FakeMappingProfile : Profile
    {
        public FakeMappingProfile()
        {
            CreateMap<SampleType, AnotherSampleType>().ReverseMap();
        }
    }
}
