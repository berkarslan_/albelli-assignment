﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Tests.Fakes.Entities
{
    public interface ISampleType
    {
        int IntProp { get; set; }
    }
}
