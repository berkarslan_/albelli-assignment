﻿using AlbelliAssignment.WebAPI.Business.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Tests.Fakes.Businesses
{
    public class FakeBusiness : BusinessBase, IFakeBusiness
    {
        public bool SampleOperation(int parameter)
        {
            return true;
        }

        public Task<bool> SampleOperationAsync(int parameter)
        {
            return Task.FromResult(true);
        }

        public void SampleVoidOperation(int parameter)
        {
        }

        public Task SampleVoidOperationAsync(int parameter)
        {
            return Task.FromResult(0);
        }
    }
}
