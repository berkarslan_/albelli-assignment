﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Tests.Fakes.Businesses
{
    public interface IFakeBusiness : IBusiness
    {
        void SampleVoidOperation(int parameter);
        bool SampleOperation(int parameter);
        Task SampleVoidOperationAsync(int parameter);
        Task<bool> SampleOperationAsync(int parameter);
    }
}
