﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.IoC.Containers;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.Tests.Fakes.Entities;

namespace AlbelliAssignment.WebAPI.Tests.IoCTests.Tests
{
    [TestClass]
    public class UnityContainerWrappedTests
    {
        private UnityContainerWrapped container;

        public UnityContainerWrappedTests()
        {
            container = new UnityContainerWrapped();
        }

        [TestMethod]
        public void CreateChildContainer_RegisterToParentResolveFromChild_CanResolve()
        {
            //Arrange
            container.Register<ISampleType, SampleType>();

            //Act
            var child = container.CreateChildContainer();
            var resolvedInstance = child.Resolve<ISampleType>();

            //Assert
            Assert.IsNotNull(resolvedInstance);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void Dispose_ResolveAfterDispose_ThrowsException()
        {
            container.Register<ISampleType, SampleType>();
            container.Dispose();
            container.Resolve<ISampleType>();
        }

        [TestMethod]
        public void Register_RegisterSampleTypeGeneric_CanRegister()
        {
            container.Register<ISampleType, SampleType>();
        }

        [TestMethod]
        public void RegisterInstance_RegisterSampleTypeInterface_CanRegister()
        {
            container.RegisterInstance<ISampleType>(new SampleType());
        }

        [TestMethod]
        public void RegisterInstance_RegisterSampleType_CanRegister()
        {
            container.RegisterInstance<SampleType>(new SampleType());
        }

        [TestMethod]
        public void RegisterNamed_RegisterSampleType_CanRegister()
        {
            container.RegisterNamed<ISampleType, SampleType>("Test");
        }

        [TestMethod]
        public void Resolve_RegisterSampleTypeAndResolve_CanResolve()
        {
            container.Register<ISampleType, SampleType>();
            var resolved = container.Resolve<ISampleType>();
            Assert.IsNotNull(resolved);
        }

        [TestMethod]
        public void Resolve_RegisterSampleTypeInstanceAndResolve_ResolvesCorrectInstance()
        {
            container.RegisterInstance<ISampleType>(new SampleType() { IntProp = 1 });
            var resolved = container.Resolve<ISampleType>();
            Assert.IsNotNull(resolved);
            Assert.AreEqual(1, resolved.IntProp);
        }

        [TestMethod]
        public void Resolve_RegisterSampleTypeAsSingletonAndResolve_ResolvesCorrectInstance()
        {
            //Arrange
            container.Register<ISampleType, SampleType>(Lifetime.Singleton);

            //Act
            var resolved1 = container.Resolve<ISampleType>();
            var resolved2 = container.Resolve<ISampleType>();

            //Assert
            Assert.AreSame(resolved1, resolved2);
        }

        [TestMethod]
        public void ResolveAll_RegisterMultipleTypesAndResolve_ResolvedCorrectly()
        {
            //Arrange
            container.RegisterNamed<ISampleType, SampleType>("Test1");
            container.RegisterNamed<ISampleType, AnotherSampleType>("Test2");

            //Act
            var resolvedObjects = container.ResolveAll(typeof(ISampleType));

            //Assert
            Assert.IsNotNull(resolvedObjects);
            Assert.IsTrue(resolvedObjects.Any(a => a.GetType() == typeof(SampleType)));
            Assert.IsTrue(resolvedObjects.Any(a => a.GetType() == typeof(AnotherSampleType)));
        }
    }
}
