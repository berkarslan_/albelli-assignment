﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.IoC.Containers;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.IoC.Base;
using Unity.Lifetime;

namespace AlbelliAssignment.WebAPI.Tests.IoCTests.Tests
{
    [TestClass]
    public class LifetimeFactoryTests
    {
        [TestMethod]
        public void GetLifetimeManager_GetLifetimeManagerForPerCall_ReturnsPerCallLifetimeManager()
        {
            var lifetimeManager = LifetimeFactory.GetLifetimeManager(Lifetime.PerCall);
            Assert.IsInstanceOfType(lifetimeManager, typeof(TransientLifetimeManager));
        }

        [TestMethod]
        public void GetLifetimeManager_GetLifetimeManagerForSingleton_ReturnsSingletonLifetimeManager()
        {
            var lifetimeManager = LifetimeFactory.GetLifetimeManager(Lifetime.Singleton);
            Assert.IsInstanceOfType(lifetimeManager, typeof(ContainerControlledLifetimeManager));
        }
    }
}
