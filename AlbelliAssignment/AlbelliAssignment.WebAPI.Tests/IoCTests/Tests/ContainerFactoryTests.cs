﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.IoC.Containers;
using AlbelliAssignment.WebAPI.IoC.Factory;

namespace AlbelliAssignment.WebAPI.Tests.IoCTests.Tests
{
    [TestClass]
    public class ContainerFactoryTests
    {
        [TestMethod]
        public void GetContainer_InitLazyInitializedContainer_ReturnsContainer()
        {
            var container = ContainerFactory.GetContainer();
            Assert.IsNotNull(container);
        }
    }
}
