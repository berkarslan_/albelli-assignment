﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using AlbelliAssignment.WebAPI.Utilities.Mapper;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.Data.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork;
using AlbelliAssignment.WebAPI.Data.UnitOfWork;
using AlbelliAssignment.WebAPI.Business.Contracts.BusinessContracts;
using AlbelliAssignment.WebAPI.Business.Business;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AutoMapper;
using AlbelliAssignment.WebAPI.Controllers.V1;
using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using System.Web.Http.Results;
using System.Net;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using AlbelliAssignment.WebAPI.Utilities.Proxy;
using System.Collections.Generic;

namespace AlbelliAssignment.WebAPI.Tests.WebAPITests
{
    [TestClass]
    public class CustomersControllerTests
    {
        private CustomersController controller;

        public CustomersControllerTests()
        {
            var container = ContainerFactory.GetContainer();
            MapperConfiguration autoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DefaultMappingProfile>();
            });
            container.RegisterInstance(autoMapperConfig);
            container.Register<Common.Contracts.Mapper.IMapper, AutoMapperWrapped>(Lifetime.Singleton);
            container.Register<IProxy, BusinessProxy>();
            container.Register<IDataContext, DataContext>();
            container.Register<IUnitOfWork, UnitOfWork>();
            container.Register<ICustomerBusiness, CustomerBusiness>();

            controller = new CustomersController();
        }

        [TestMethod]
        public async Task GetCustomerById_ForExistingCustomer_ReturnsCustomer()
        {
            //Arrange
            var dto = new CustomerInfoDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            var savedDto = await controller.Post(dto) as CreatedAtRouteNegotiatedContentResult<CustomerDTO>;

            //Act
            var result = await controller.GetCustomerById(savedDto.Content.Id);

            //Assert
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<CustomerDTO>));
        }

        [TestMethod]
        public async Task GetCustomerById_ForNonExistingCustomer_ReturnsNotFound()
        {
            var result = await controller.GetCustomerById(0);
            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }

        [TestMethod]
        public async Task GetCustomers_GetAllCustomers_ReturnsCustomers()
        {
            var result = await controller.GetCustomers();
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<List<CustomerInfoDTO>>));
        }

        [TestMethod]
        public async Task Post_SaveValidCustomer_ReturnsOk()
        {
            //Arrange
            var dto = new CustomerInfoDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };

            //Act
            var result = await controller.Post(dto);

            //Assert
            Assert.IsInstanceOfType(result, typeof(CreatedAtRouteNegotiatedContentResult<CustomerDTO>));
        }

        [TestMethod]
        public async Task Post_SaveExistingCustomer_ReturnsBadRequest()
        {
            //Arrange
            var dto = new CustomerInfoDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };

            //Act
            var firstResult = await controller.Post(dto);
            var secondResult = await controller.Post(dto);

            //Assert
            Assert.IsInstanceOfType(firstResult, typeof(CreatedAtRouteNegotiatedContentResult<CustomerDTO>));
            Assert.IsInstanceOfType(secondResult, typeof(BadRequestErrorMessageResult));
        }

        //TODO: ModelStateValidateAttribute is not working so that it skips the ModelState validation and receives error in db side. Make the attribute work in unit tests
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        [TestMethod]
        public async Task Post_SaveNotValidCustomer_ReturnsOk()
        {
            //Arrange
            var dto = new CustomerInfoDTO()
            {
                Name = Faker.Name.FullName()
            };

            //Act
            var result = await controller.Post(dto);

            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public async Task Orders_SaveValidOrder_ReturnsOk()
        {
            //Arrange
            var customerDto = new CustomerInfoDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            var savedCustomerDto = await controller.Post(customerDto) as CreatedAtRouteNegotiatedContentResult<CustomerDTO>;
            var dto = new OrderInfoDTO()
            {
                Price = 10
            };

            //Act
            var result = await controller.Orders(savedCustomerDto.Content.Id, dto);

            //Assert
            Assert.IsInstanceOfType(result, typeof(CreatedAtRouteNegotiatedContentResult<OrderDTO>));
        }

        //TODO: ModelStateValidateAttribute is not working so that it skips the ModelState validation and receives error in db side. Make the attribute work in unit tests
        [ExpectedException(typeof(Exception), AllowDerivedTypes = true)]
        [TestMethod]
        public async Task Orders_SaveValidOrderForNonExistingCustomer_ReturnsBadRequest()
        {
            //Arrange
            var dto = new OrderInfoDTO()
            {
                Price = 10
            };

            //Act
            var result = await controller.Orders(0, dto);

            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }
    }
}
