﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.Common.Helpers;

namespace AlbelliAssignment.WebAPI.Tests.CommonTests
{
    [TestClass]
    public class RandomGeneratorTests
    {
        [TestMethod]
        public void GetRandomNumber_BetweenMinus100Positive100_ReturnsInRange()
        {
            var number = RandomGenerator.GetRandomNumber(-100, 100);
            Assert.IsTrue(number >= -100 && number <= 100);
        }

        [TestMethod]
        public void GetRandomNumber_ZeroRange_ReturnsZero()
        {
            var number = RandomGenerator.GetRandomNumber(0, 0);
            Assert.AreEqual(0, number);
        }

        [TestMethod]
        public void GetRandomNumber_GetTwoNumbersConsecutively_ReturnsDifferentNumbers()
        {
            var first = RandomGenerator.GetRandomNumber(int.MinValue, int.MaxValue);
            var second = RandomGenerator.GetRandomNumber(int.MinValue, int.MaxValue);
            Assert.AreNotEqual(first, second);
        }
    }
}
