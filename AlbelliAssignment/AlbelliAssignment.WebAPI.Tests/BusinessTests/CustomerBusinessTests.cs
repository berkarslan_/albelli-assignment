﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using AlbelliAssignment.WebAPI.Utilities.Proxy;
using AlbelliAssignment.WebAPI.Business.Contracts.BusinessContracts;
using AlbelliAssignment.WebAPI.Business.Business;
using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using FizzWare.NBuilder;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Common.Contracts.Mapper;
using AlbelliAssignment.WebAPI.Utilities.Mapper;
using AlbelliAssignment.WebAPI.IoC.Base;
using AutoMapper;
using AlbelliAssignment.WebAPI.Data.Contracts.Context;
using AlbelliAssignment.WebAPI.Data.Context;
using AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork;
using AlbelliAssignment.WebAPI.Data.UnitOfWork;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace AlbelliAssignment.WebAPI.Tests.BusinessTests
{
    [TestClass]
    public class CustomerBusinessTests
    {
        private ICustomerBusiness customerBusiness;

        public CustomerBusinessTests()
        {
            var container = ContainerFactory.GetContainer();
            MapperConfiguration autoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<DefaultMappingProfile>();
            });
            container.RegisterInstance(autoMapperConfig);
            container.Register<Common.Contracts.Mapper.IMapper, AutoMapperWrapped>(Lifetime.Singleton);
            container.Register<IDataContext, DataContext>();
            container.Register<IUnitOfWork, UnitOfWork>();
            customerBusiness = new CustomerBusiness();
        }

        [TestMethod]
        public async Task AddCustomer_CreateValidRandomizedCustomer_Created()
        {
            //Arrange
            var dto = new CustomerDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };

            //Act
            await customerBusiness.AddCustomer(dto);

            //Assert
            Assert.AreNotEqual(0, dto.Id);
        }

        [ExpectedException(typeof(DbEntityValidationException))]
        [TestMethod]
        public async Task AddCustomer_CreateNotValidRandomizedCustomer_ThrowsException()
        {
            //Arrange
            var dto = new CustomerDTO()
            {
                Name = Faker.Name.FullName()
            };

            //Act & Assert
            await customerBusiness.AddCustomer(dto);
        }

        [TestMethod]
        public async Task AddOrder_CreateValidRandomizedOrderForCustomer_Created()
        {
            //Arrange
            var customerDto = new CustomerDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            await customerBusiness.AddCustomer(customerDto);
            var orderDto = new OrderDTO()
            {
                CreatedDate = DateTime.Now,
                Price = 10,
                CustomerId = customerDto.Id
            };

            //Act
            await customerBusiness.AddOrder(orderDto);

            //Assert
            Assert.AreNotEqual(0, orderDto.Id);
        }

        [ExpectedException(typeof(DbUpdateException))]
        [TestMethod]
        public async Task AddOrder_CreateNotValidRandomizedOrder_ThrowsException()
        {
            //Arrange
            var customerDto = new CustomerDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            await customerBusiness.AddCustomer(customerDto);
            var orderDto = new OrderDTO()
            {
                Price = 10,
                CustomerId = customerDto.Id
            };

            //Act & Assert
            await customerBusiness.AddOrder(orderDto);
        }

        [ExpectedException(typeof(DbUpdateException))]
        [TestMethod]
        public async Task AddOrder_CreateValidRandomizedOrderForNonExistingCustomer_ThrowsException()
        {
            //Arrange
            var orderDto = new OrderDTO()
            {
                CreatedDate = DateTime.Now,
                Price = 10
            };

            //Act & Assert
            await customerBusiness.AddOrder(orderDto);
        }

        [TestMethod]
        public async Task CustomerExists_ExistingCustomer_ReturnsTrue()
        {
            var dto = new CustomerDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            await customerBusiness.AddCustomer(dto);

            //Act
            var result = await customerBusiness.CustomerExists(dto.Email);

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task CustomerExists_NotExistingCustomer_ReturnsFalse()
        {
            var result = await customerBusiness.CustomerExists("");
            Assert.IsFalse(result);
        }

        [TestMethod]
        public async Task GetCustomer_ExistingCustomer_ReturnsCustomer()
        {
            //Arrange
            var dto = new CustomerDTO()
            {
                Email = Faker.Internet.Email(),
                Name = Faker.Name.FullName()
            };
            await customerBusiness.AddCustomer(dto);

            //Act
            var customer = await customerBusiness.GetCustomer(dto.Id);

            //Assert
            Assert.IsNotNull(customer);
        }

        [TestMethod]
        public async Task GetCustomer_NotExistingCustomer_ThrowsException()
        {
            var customer = await customerBusiness.GetCustomer(0);
            Assert.IsNull(customer);
        }

        [TestMethod]
        public async Task GetCustomers_GetAllCustomers_WorksSuccessfully()
        {
            var customers = await customerBusiness.GetCustomers();
            Assert.IsNotNull(customers);
        }
    }
}
