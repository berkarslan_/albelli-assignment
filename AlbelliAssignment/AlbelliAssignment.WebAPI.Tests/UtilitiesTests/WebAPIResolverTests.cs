﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unity;
using AlbelliAssignment.WebAPI.Utilities.IoC;
using AlbelliAssignment.WebAPI.IoC.Containers;
using AlbelliAssignment.WebAPI.Tests.Fakes.Entities;
using System.Linq;
using AlbelliAssignment.WebAPI.IoC.Base;

namespace AlbelliAssignment.WebAPI.Tests.UtilitiesTests
{
    [TestClass]
    public class WebAPIResolverTests
    {
        IContainer container;

        public WebAPIResolverTests()
        {
            container = new UnityContainerWrapped();
        }

        [TestMethod]
        public void GetService_GetARegisteredService_ReturnsService()
        {
            //Arrange
            container.Register<ISampleType, SampleType>();

            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            var resolved = resolver.GetService(typeof(ISampleType));

            //Assert
            Assert.IsNotNull(resolved);
        }

        [TestMethod]
        public void GetService_GetANonRegisteredService_ReturnsNull()
        {
            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            var resolved = resolver.GetService(typeof(ISampleType));

            //Assert
            Assert.IsNull(resolved);
        }

        [TestMethod]
        public void GetServices_GetRegisteredServices_ReturnsServices()
        {
            //Arrange
            container.RegisterNamed<ISampleType, SampleType>("1");
            container.RegisterNamed<ISampleType, AnotherSampleType>("2");

            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            var resolvedObjects = resolver.GetServices(typeof(ISampleType));

            //Assert
            Assert.IsNotNull(resolvedObjects);
            Assert.IsTrue(resolvedObjects.Any(a => a.GetType() == typeof(SampleType)));
            Assert.IsTrue(resolvedObjects.Any(a => a.GetType() == typeof(AnotherSampleType)));
        }

        [TestMethod]
        public void GetServices_GetNonRegisteredServices_ReturnsEmptyList()
        {
            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            var resolvedObjects = resolver.GetServices(typeof(ISampleType));

            //Assert
            Assert.IsNotNull(resolvedObjects);
            Assert.IsFalse(resolvedObjects.Any());
        }

        [TestMethod]
        public void BeginScope_GetScopedResolver_ReturnsScopedResolver()
        {
            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            var scope = resolver.BeginScope();

            //Assert
            Assert.IsNotNull(scope);
        }

        [ExpectedException(typeof(ArgumentNullException))]
        [TestMethod]
        public void Dispose_DisposeAndTryToGetService_ThrowsException()
        {
            //Arrange
            container.Register<ISampleType, SampleType>();

            //Act
            WebAPIResolver resolver = new WebAPIResolver(container);
            resolver.Dispose();

            //Assert
            resolver.GetService(typeof(ISampleType));
        }
    }
}
