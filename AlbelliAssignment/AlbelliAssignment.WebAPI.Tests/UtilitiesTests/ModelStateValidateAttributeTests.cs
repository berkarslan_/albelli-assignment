﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.Utilities.Attributes.FilterAttributes;
using System.Web.Http.Controllers;
using AlbelliAssignment.WebAPI.Tests.Utils;
using Moq;
using System.Net.Http;
using System.Net;

namespace AlbelliAssignment.WebAPI.Tests.UtilitiesTests
{
    [TestClass]
    public class ModelStateValidateAttributeTests
    {
        [TestMethod]
        public void OnActionExecuting_ValidateForGetRequest_DoesntReturnError()
        {
            //Arrange
            var actionContext = ContextUtil.CreateActionContext();

            //Act
            var attr = new ModelStateValidateAttribute();
            attr.OnActionExecuting(actionContext);

            //Assert
            Assert.IsNull(actionContext.Response);
        }

        [TestMethod]
        public void OnActionExecuting_ValidateForValidPostRequest_DoesntReturnError()
        {
            //Arrange
            var actionContext = ContextUtil.CreateActionContext();
            actionContext.ControllerContext.Request.Method = new HttpMethod("POST");

            //Act
            var attr = new ModelStateValidateAttribute();
            attr.OnActionExecuting(actionContext);

            //Assert
            Assert.IsNull(actionContext.Response);
        }

        [TestMethod]
        public void OnActionExecuting_ValidateForNotValidPostRequest_ReturnsError()
        {
            //Arrange
            var actionContext = ContextUtil.CreateActionContext();
            actionContext.ControllerContext.Request.Method = new HttpMethod("POST");
            actionContext.ModelState.AddModelError("FooError", "Foo Message");

            //Act
            var attr = new ModelStateValidateAttribute();
            attr.OnActionExecuting(actionContext);

            //Assert
            Assert.IsNotNull(actionContext.Response);
            Assert.AreEqual(HttpStatusCode.BadRequest, actionContext.Response.StatusCode);
        }
    }
}
