﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using AlbelliAssignment.WebAPI.Utilities.Proxy;
using AlbelliAssignment.WebAPI.Tests.Fakes.Businesses;
using System.Threading.Tasks;
using AlbelliAssignment.WebAPI.IoC.Base;

namespace AlbelliAssignment.WebAPI.Tests.UtilitiesTests
{
    [TestClass]
    public class BusinessProxyTests
    {
        private IContainer container;

        public BusinessProxyTests()
        {
            container = ContainerFactory.GetContainer();
        }

        [TestMethod]
        public void Execute_CallAVoidSampleBusiness_CallsSuccessfully()
        {
            //Arrange
            container.Register<IFakeBusiness, FakeBusiness>();

            //Act & Assert
            var proxy = new BusinessProxy();
            proxy.Execute<IFakeBusiness>(a => a.SampleVoidOperation(0));
        }

        [TestMethod]
        public async Task Execute_CallAnAsyncVoidSampleBusiness_CallsSuccessfully()
        {
            //Arrange
            container.Register<IFakeBusiness, FakeBusiness>();

            //Act & Assert
            var proxy = new BusinessProxy();
            await proxy.ExecuteAsync<IFakeBusiness>(a => a.SampleVoidOperationAsync(0));
        }

        [TestMethod]
        public void Execute_CallASampleBusinessWithReturnValue_CallsSuccessfully()
        {
            //Arrange
            container.Register<IFakeBusiness, FakeBusiness>();

            //Act
            var proxy = new BusinessProxy();
            var result = proxy.Execute<IFakeBusiness, bool>(a => a.SampleOperation(0));

            //Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public async Task Execute_CallAnAsyncVoidSampleBusinessWithReturnValue_CallsSuccessfully()
        {
            //Arrange
            container.Register<IFakeBusiness, FakeBusiness>();

            //Act
            var proxy = new BusinessProxy();
            var result = await proxy.ExecuteAsync<IFakeBusiness, bool>(a => a.SampleOperationAsync(0));

            //Assert
            Assert.IsTrue(result);
        }
    }
}
