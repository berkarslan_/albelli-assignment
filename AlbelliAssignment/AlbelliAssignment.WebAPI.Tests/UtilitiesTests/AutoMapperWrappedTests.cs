﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using AlbelliAssignment.WebAPI.Utilities.Mapper;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.Tests.Fakes.Mapping;
using AlbelliAssignment.WebAPI.IoC.Containers;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Tests.Fakes.Entities;

namespace AlbelliAssignment.WebAPI.Tests.UtilitiesTests
{
    [TestClass]
    public class AutoMapperWrappedTests
    {
        [TestMethod]
        public void Map_MapSampleTypeToAnotherSampleType_ReturnsAnotherSampleType()
        {
            //Arrange
            var container = ContainerFactory.GetContainer();
            MapperConfiguration autoMapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<FakeMappingProfile>();
            });
            container.RegisterInstance(autoMapperConfig);
            container.Register<Common.Contracts.Mapper.IMapper, AutoMapperWrapped>(Lifetime.Singleton);
            var sampleType = new SampleType() { IntProp = 1 };

            //Act
            AutoMapperWrapped wrappedMapper = new AutoMapperWrapped();
            var anotherSampleType = wrappedMapper.Map<SampleType, AnotherSampleType>(sampleType);

            //Assert
            Assert.IsNotNull(anotherSampleType);
            Assert.AreEqual(1, anotherSampleType.IntProp);
        }
    }
}
