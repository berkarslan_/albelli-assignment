﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AlbelliAssignment.WebAPI.Utilities.Registrations;
using Moq;
using Swashbuckle.Application;
using System.Web.Http;

namespace AlbelliAssignment.WebAPI.Tests.UtilitiesTests
{
    [TestClass]
    public class RegistrarTests
    {
        [TestMethod]
        public void RegisterGlobal_WebAPIRegistrations_WorksSuccessfully()
        {
            //Arrange
            var config = new HttpConfiguration();

            //Act & Assert
            //Swagger is too coupled with Web API so that it can't be tested
            Registrar.LoadNotUsedDirectlyAssemblies();
            config.AddRoutingWithVersioning()
                .AddFilters();
            Registrar.ConfigureDIRegistrations();
        }
    }
}
