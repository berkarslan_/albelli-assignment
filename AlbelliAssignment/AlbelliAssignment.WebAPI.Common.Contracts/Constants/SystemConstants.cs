﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Common.Contracts.Constants
{
    /// <summary>
    /// System constants that can be used widely
    /// </summary>
    public static class SystemConstants
    {
        /// <summary>
        /// This class corresponds to appSettings element in web.config
        /// </summary>
        public static class AppSettings
        {
            public static readonly string ClientId = ConfigurationManager.AppSettings["ClientId"];
            public static readonly string ClientSecret = ConfigurationManager.AppSettings["ClientSecret"];
            public static readonly string DefaultConnectionName = ConfigurationManager.AppSettings["DefaultConnectionName"];
        }
    }
}
