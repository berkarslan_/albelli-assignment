﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Common.Contracts.Constants
{
    /// <summary>
    /// Web API Route Url Table
    /// </summary>
    public static class RouteUrlTable
    {
        public static class Customer
        {
            public const string RoutePrefix = "v{version:apiVersion}/Customers";

            public const string GetCustomerById = "{id:int}";
            public const string GetCustomers = "";
            public const string SaveCustomer = "";
            public const string SaveOrderForCustomer = "{id:int}/Orders";
        }

        public static class OAuth
        {
            public const string AuthorizePath = "/OAuth/Authorize";
            public const string TokenPath = "/OAuth/Token";
            public const string AuthorizationServerBaseAddress = "http://localhost:8523";
            public const string ResourceServerBaseAddress = "http://localhost:27675";
        }
    }
}
