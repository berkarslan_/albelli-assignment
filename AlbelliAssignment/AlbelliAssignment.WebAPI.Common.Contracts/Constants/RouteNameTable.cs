﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Common.Contracts.Constants
{
    /// <summary>
    /// Web API Route Name Table
    /// </summary>
    public static class RouteNameTable
    {
        public static class Customer
        {
            public const string GetCustomerById = "GetCustomerById";
            public const string GetCustomers = "GetCustomers";
            public const string SaveCustomer = "SaveCustomer";
            public const string SaveOrderForCustomer = "SaveCustomerOrder";
        }
    }
}
