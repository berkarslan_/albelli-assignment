﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Common.Contracts.Mapper
{
    /// <summary>
    /// Property mapper interface
    /// </summary>
    public interface IMapper
    {
        /// <summary>
        /// Map a <typeparam name="TFrom"/> typed <param name="value" /> instance to <typeparam name="TTo"/> type
        /// </summary>
        /// <typeparam name="TFrom">Type to convert from</typeparam>
        /// <typeparam name="TTo">Type to convert to</typeparam>
        /// <param name="value">Value to convert</param>
        /// <returns>Converted instance</returns>
        TTo Map<TFrom, TTo>(TFrom value);
    }
}
