﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using AlbelliAssignment.WebAPI.Common.Contracts.Mapper;
using AlbelliAssignment.WebAPI.Data.Contracts.UnitOfWork;
using AlbelliAssignment.WebAPI.IoC.Base;
using AlbelliAssignment.WebAPI.IoC.Factory;
using AlbelliAssignment.WebAPI.Utilities.Contracts.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Business.Base
{
    /// <summary>
    /// Business classes' base which contains useful objects
    /// </summary>
    public abstract class BusinessBase : IBusiness
    {
        private readonly IContainer container = ContainerFactory.GetContainer();

        private IProxy proxy;
        /// <summary>
        /// Proxy to access resource
        /// </summary>
        protected IProxy Proxy
        {
            get
            {
                if (proxy == null)
                {
                    proxy = container.Resolve<IProxy>();
                }
                return proxy;
            }
        }

        private IMapper mapper;
        /// <summary>
        /// Mapper to map DTOs & Entities
        /// </summary>
        protected IMapper Mapper
        {
            get
            {
                if (mapper == null)
                {
                    mapper = container.Resolve<IMapper>();
                }
                return mapper;
            }
        }

        private IUnitOfWork unitOfWork;
        /// <summary>
        /// UnitOfWork to manage the data
        /// </summary>
        protected IUnitOfWork UnitOfWork
        {
            get
            {
                if (unitOfWork == null)
                {
                    unitOfWork = container.Resolve<IUnitOfWork>();
                }
                return unitOfWork;
            }
        }
    }
}
