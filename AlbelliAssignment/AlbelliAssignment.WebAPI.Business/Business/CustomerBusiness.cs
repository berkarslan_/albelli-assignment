﻿using AlbelliAssignment.WebAPI.Business.Contracts.BusinessContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using AlbelliAssignment.WebAPI.Business.Base;
using AlbelliAssignment.WebAPI.Data.Entities.Entities;
using System.Linq.Expressions;

namespace AlbelliAssignment.WebAPI.Business.Business
{
    public class CustomerBusiness : BusinessBase, ICustomerBusiness
    {
        public async Task AddCustomer(CustomerDTO dto)
        {
            var customer = Mapper.Map<CustomerInfoDTO, Customer>(dto);
            UnitOfWork.Repository<Customer>().Insert(customer);
            await UnitOfWork.SaveChangesAsync();
            dto.Id = customer.Id;
        }

        public async Task AddOrder(OrderDTO dto)
        {
            var order = Mapper.Map<OrderDTO, Order>(dto);
            UnitOfWork.Repository<Order>().Insert(order);
            await UnitOfWork.SaveChangesAsync();
            dto.Id = order.Id;
        }

        public async Task<bool> CustomerExists(string customerEmail)
        {
            return await UnitOfWork.Repository<Customer>().AnyAsync(a => a.Email == customerEmail);
        }

        public async Task<CustomerDTO> GetCustomer(int id)
        {
            var customer = await UnitOfWork.Repository<Customer>()
                .Get(g => g.Id == id, includeProperties: new List<Expression<Func<Customer, object>>>() { a => a.Orders })
                .SingleOrDefaultAsync();
            var customerDto = Mapper.Map<Customer, CustomerDTO>(customer);
            return customerDto;
        }

        public async Task<List<CustomerInfoDTO>> GetCustomers()
        {
            var customers = await UnitOfWork.Repository<Customer>().ToListAsync();
            var customerDtos = Mapper.Map<List<Customer>, List<CustomerInfoDTO>>(customers);
            return customerDtos;
        }
    }
}
