﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Business.Contracts.DTOs
{
    public class OrderInfoDTO
    {
        public int Id { get; set; }

        [Required]
        [Range(0.0, double.MaxValue)]
        public decimal Price { get; set; }
    }
}
