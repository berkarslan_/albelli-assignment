﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Business.Contracts.DTOs
{
    public class CustomerDTO : CustomerInfoDTO
    {
        public List<OrderDTO> Orders { get; set; }
    }
}
