﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Business.Contracts.DTOs
{
    public class OrderDTO : OrderInfoDTO
    {
        [Required]
        public int CustomerId { get; set; }

        [Required]
        public DateTime? CreatedDate { get; set; }
    }
}
