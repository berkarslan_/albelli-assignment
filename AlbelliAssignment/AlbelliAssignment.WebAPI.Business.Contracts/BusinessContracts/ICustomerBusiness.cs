﻿using AlbelliAssignment.WebAPI.Business.Contracts.Base;
using AlbelliAssignment.WebAPI.Business.Contracts.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Business.Contracts.BusinessContracts
{
    /// <summary>
    /// Interface for customer business
    /// </summary>
    public interface ICustomerBusiness : IBusiness
    {
        /// <summary>
        /// Checks whether a customer with the same email exists or not
        /// </summary>
        /// <param name="customerEmail">Email address to check against</param>
        /// <returns>True if exists; false if not</returns>
        Task<bool> CustomerExists(string customerEmail);

        /// <summary>
        /// Gets basic customer information
        /// </summary>
        /// <returns>Basic customer information</returns>
        Task<List<CustomerInfoDTO>> GetCustomers();

        /// <summary>
        /// Gets detailed customer information
        /// </summary>
        /// <param name="id">Customer id to get</param>
        /// <returns>Detailed customer information (customer info, orders etc.)</returns>
        Task<CustomerDTO> GetCustomer(int id);

        /// <summary>
        /// Saves customer
        /// </summary>
        /// <param name="dto">Customer to save</param>
        /// <returns>Task</returns>
        Task AddCustomer(CustomerDTO dto);

        /// <summary>
        /// Saves order for a customer
        /// </summary>
        /// <param name="dto">Customer order to save</param>
        /// <returns>Task</returns>
        Task AddOrder(OrderDTO dto);
    }
}
