﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.OAuth;
using AlbelliAssignment.WebAPI.Common.Contracts.Constants;
using AlbelliAssignment.DummyAuthorizationServer.Security;
using Microsoft.Owin.Cors;
using System.Web.Cors;
using AlbelliAssignment.DummyAuthorizationServer.Constants;

[assembly: OwinStartup(typeof(AlbelliAssignment.DummyAuthorizationServer.Startup))]

namespace AlbelliAssignment.DummyAuthorizationServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //Setup Cors
            app.UseCors(new CorsOptions()
            {
                PolicyProvider = new CorsPolicyProvider()
                {
                    PolicyResolver = context => 
                    {
                        var corsPolicy = new CorsPolicy
                        {
                            AllowAnyMethod = true,
                            AllowAnyHeader = true
                        };

                        var origins = AuthConstants.DummyCorsDomains;
                        if (origins.Count != 0)
                        {
                            foreach (var origin in origins)
                            {
                                corsPolicy.Origins.Add(origin);
                            }
                        }
                        else
                        {
                            corsPolicy.AllowAnyOrigin = true;
                        }

                        return Task.FromResult(corsPolicy);
                    }
                }
            });

            //Setup Authorization Server
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AuthorizeEndpointPath = new PathString(RouteUrlTable.OAuth.AuthorizePath),
                TokenEndpointPath = new PathString(RouteUrlTable.OAuth.TokenPath),
                ApplicationCanDisplayErrors = true,
                AllowInsecureHttp = true,
                Provider = new AlbelliOAuthAuthorizationServerProvider(),
                RefreshTokenProvider = new AlbelliRefreshTokenProvider()
            });
        }
    }
}
