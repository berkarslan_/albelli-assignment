﻿using AlbelliAssignment.DummyAuthorizationServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlbelliAssignment.DummyAuthorizationServer.Constants
{
    internal static class AuthConstants
    {
        public static readonly Client DummyClient = new Client
        {
            Id = "Albelli",
            Secret = "Secret"
        };

        public static readonly string DummyPassword = "123456";

        public static readonly List<string> DummyCorsDomains = new List<string>()
        {
            "http://localhost:27675"
        };
    }
}