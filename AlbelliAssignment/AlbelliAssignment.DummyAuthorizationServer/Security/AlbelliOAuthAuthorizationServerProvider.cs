﻿using AlbelliAssignment.DummyAuthorizationServer.Constants;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.DummyAuthorizationServer.Security
{
    internal class AlbelliOAuthAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            //Validate Client Id and Client Secret
            string clientId, clientSecret;

            if (context.TryGetBasicCredentials(out clientId, out clientSecret) ||
                context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                if (clientId == AuthConstants.DummyClient.Id && clientSecret == AuthConstants.DummyClient.Secret)
                {
                    context.Validated();
                }
            }

            return Task.FromResult(0);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //Validate user credentials
            if (context.Password == AuthConstants.DummyPassword)
            {
                //Assign the ticket
                var identity = new ClaimsIdentity(new GenericIdentity(context.UserName, OAuthDefaults.AuthenticationType), context.Scope.Select(x => new Claim("urn:oauth:scope", x)));
                context.Validated(identity);
            }

            return Task.FromResult(0);
        }
    }
}
