﻿using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Entities.Entities
{
    public class Customer : ModelBase
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(100)]
        [Index("IX_Email", 1, IsUnique = true)]
        public string Email { get; set; }

        //Navigation Properties
        public virtual List<Order> Orders { get; set; }
    }
}
