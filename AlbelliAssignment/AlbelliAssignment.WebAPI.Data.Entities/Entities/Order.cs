﻿using AlbelliAssignment.WebAPI.Data.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlbelliAssignment.WebAPI.Data.Entities.Entities
{
    public class Order : ModelBase
    {
        [Required]
        public decimal Price { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public int CustomerId { get; set; }

        //Navigation Properties
        public virtual Customer Customer { get; set; }
    }
}
